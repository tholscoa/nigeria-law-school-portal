<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\ApplicationsTables;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function adminViewOne($id){
        $applicant = ApplicationsTables::find($id);
        return view('application.printout', compact('applicant'));
    }

    public function admin(){
        $all_apps = ApplicationsTables::all();
        return view('admin.index', compact('all_apps'));
    }

    public function preForm(){
        return view('application.preform');
    }

    public function generateAppNo(Request $request){
        $applicant = new Application;
        $applicant->app_no = strtoupper('NLS/'.date('Y').'/'. Str::random(4));
        $applicant->name = strtoupper($request->input('name'));
        $applicant->email = $request->input('email');
        $applicant->phone = $request->input('phone');
        $applicant->date = date('Y');
        $applicant->save();

        $states = NIG_STATE;

        $applicant = Application::where('app_no', $applicant->app_no)->first();
        return view('application.form', compact('applicant', 'states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $applicant = new ApplicationsTables;
        $applicant->name = $request->input('name');
        $applicant->app_no = $request->input('app_no');
        $applicant->applied_before = $request->input('applied_before');
        $applicant->prev_period = $request->input('prev_period');
        $applicant->perm_addr = $request->input('perm_addr');
        $applicant->pres_cont_addr = $request->input('pres_cont_addr');
        $applicant->tel_no = $request->input('tel_no');
        $applicant->email = $request->input('email');
        $applicant->state = $request->input('state');
        $applicant->lga = $request->input('lga');
        $applicant->hometown = $request->input('hometown');
        $applicant->homeaddr = $request->input('homeaddr');
        $applicant->nationality = $request->input('nationality');
        $applicant->dob = $request->input('dob');
        $applicant->marital = $request->input('marital');
        $applicant->nokname = $request->input('nokname');
        $applicant->noktel = $request->input('noktel');
        $applicant->nokaddr = $request->input('nokaddr');
        $applicant->campus_choice_1 = $request->input('campus_choice_1');
        $applicant->campus_choice_2 = $request->input('campus_choice_2');
        $applicant->campus_choice_3 = $request->input('campus_choice_3');
        $applicant->campus_choice_4 = $request->input('campus_choice_4');
        $applicant->campus_choice_5 = $request->input('campus_choice_5');
        $applicant->campus_choice_6 = $request->input('campus_choice_6');
        $applicant->choicereason = $request->input('choicereason');
        $applicant->sec_sch_name = $request->input('sec_sch_name');
        $applicant->sec_sch_date_from = $request->input('sec_sch_date_from');
        $applicant->sec_sch_date_to = $request->input('sec_sch_date_to');
        $applicant->sec_sch_cert = $request->input('sec_sch_cert');
        $applicant->sec_sch_sub1 = $request->input('sec_sch_sub1');
        $applicant->sec_sch_sub2 = $request->input('sec_sch_sub2');
        $applicant->sec_sch_sub3 = $request->input('sec_sch_sub3');
        $applicant->sec_sch_sub4 = $request->input('sec_sch_sub4');
        $applicant->sec_sch_sub5 = $request->input('sec_sch_sub5');
        $applicant->sec_sch_sub6 = $request->input('sec_sch_sub6');
        $applicant->sec_sch_sub7 = $request->input('sec_sch_sub7');
        $applicant->sec_sch_sub8 = $request->input('sec_sch_sub8');
        $applicant->sec_sch_sub9 = $request->input('sec_sch_sub9');
        $applicant->entry_qualifications = $request->input('entry_qualifications');
        $applicant->first_post_sec_sch_name = $request->input('first_post_sec_sch_name');
        $applicant->first_post_sec_sch_date_from = $request->input('first_post_sec_sch_date_from');
        $applicant->first_post_sec_sch_date_to = $request->input('first_post_sec_sch_date_to');
        $applicant->first_post_sec_sch_title = $request->input('first_post_sec_sch_title');
        $applicant->first_post_sec_sch_cert = $request->input('first_post_sec_sch_cert');
        $applicant->first_post_sec_sch_year_obtained = $request->input('first_post_sec_sch_year_obtained');
        $applicant->second_post_sec_sch_name = $request->input('second_post_sec_sch_name');
        $applicant->second_post_sec_sch_date_from = $request->input('second_post_sec_sch_date_from');
        $applicant->second_post_sec_sch_date_to = $request->input('second_post_sec_sch_date_to');
        $applicant->second_post_sec_sch_title = $request->input('second_post_sec_sch_title');
        $applicant->second_post_sec_sch_cert = $request->input('second_post_sec_sch_cert');
        $applicant->second_post_sec_sch_year_obtained = $request->input('second_post_sec_sch_year_obtained');
        $applicant->third_post_sec_sch_name = $request->input('third_post_sec_sch_name');
        $applicant->third_post_sec_sch_date_from = $request->input('third_post_sec_sch_date_from');
        $applicant->third_post_sec_sch_date_to = $request->input('third_post_sec_sch_date_to');
        $applicant->third_post_sec_sch_title = $request->input('third_post_sec_sch_title');
        $applicant->third_post_sec_sch_cert = $request->input('third_post_sec_sch_cert');
        $applicant->third_post_sec_sch_year_obtained = $request->input('third_post_sec_sch_year_obtained');
        $applicant->first_emploment_history_date_from = $request->input('first_emploment_history_date_from');
        $applicant->first_emploment_history_date_to = $request->input('first_emploment_history_date_to');
        $applicant->first_emploment_history_post_held = $request->input('first_emploment_history_post_held');
        $applicant->first_emploment_history_employer = $request->input('first_emploment_history_employer');
        $applicant->first_emploment_history_employer_address = $request->input('first_emploment_history_employer_address');
        $applicant->second_emploment_history_date_from = $request->input('second_emploment_history_date_from');
        $applicant->second_emploment_history_date_to = $request->input('second_emploment_history_date_to');
        $applicant->second_emploment_history_post_held = $request->input('second_emploment_history_post_held');
        $applicant->second_emploment_history_employer = $request->input('second_emploment_history_employer');
        $applicant->second_emploment_history_employer_address = $request->input('second_emploment_history_employer_address');
        $applicant->third_emploment_history_date_from = $request->input('third_emploment_history_date_from');
        $applicant->third_emploment_history_date_to = $request->input('third_emploment_history_date_to');
        $applicant->third_emploment_history_post_held = $request->input('third_emploment_history_post_held');
        $applicant->third_emploment_history_employer = $request->input('third_emploment_history_employer');
        $applicant->third_emploment_history_employer_address = $request->input('third_emploment_history_employer_address');
        $applicant->first_unemploment_history_date_from = $request->input('first_unemploment_history_date_from');
        $applicant->first_unemploment_history_date_to = $request->input('first_unemploment_history_date_to');
        $applicant->first_unemploment_history_where_about = $request->input('first_unemploment_history_where_about');
        $applicant->first_unemploment_history_course_title = $request->input('first_unemploment_history_course_title');
        $applicant->second_unemploment_history_date_from = $request->input('second_unemploment_history_date_from');
        $applicant->second_unemploment_history_date_to = $request->input('second_unemploment_history_date_to');
        $applicant->second_unemploment_history_where_about = $request->input('second_unemploment_history_where_about');
        $applicant->second_unemploment_history_course_title = $request->input('second_unemploment_history_course_title');
        $applicant->second_unemploment_history_class = $request->input('second_unemploment_history_class');
        $applicant->third_unemploment_history_date_from = $request->input('third_unemploment_history_date_from');
        $applicant->third_unemploment_history_date_to = $request->input('third_unemploment_history_date_to');
        $applicant->third_unemploment_history_where_about = $request->input('third_unemploment_history_where_about');
        $applicant->third_unemploment_history_course_title = $request->input('third_unemploment_history_course_title');
        $applicant->third_unemploment_history_class = $request->input('third_unemploment_history_class');
        $applicant->save();



        return view('application.printout', compact('applicant'));
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show(Application $application)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function edit(Application $application)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Application $application)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        //
    }
}
