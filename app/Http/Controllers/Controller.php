<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;




$states = [
    ""=>"" ,
    "Abia" => "Abia",
    "Adamawa" => "Adamawa",
    "Anambra" => "Anambra",
    "Akwa Ibom" => "Akwa Ibom",
    "Bauchi" => "Bauchi",
    "Bayelsa" => "Bayelsa",
    "Benue" => "Benue",
    "Borno" => "Borno",
    "Cross River" => "Cross River",
    "Delta" => "Delta",
    "Ebonyi" => "Ebonyi",
    "Enugu" => "Enugu",
    "Edo" => "Edo",
    "Ekiti" => "Ekiti",
    "FCT - Abuja" => "FCT - Abuja",
    "Gombe" => "Gombe",
    "Imo" => "Imo",
    "Jigawa" => "Jigawa",
    "Kaduna" => "Kaduna",
    "Kano" => "Kano",
    "Katsina" => "Katsina",
    "Kebbi" => "Kebbi",
    "Kogi" => "Kogi",
    "Kwara" => "Kwara",
    "Lagos" => "Lagos",
    "Nasarawa" => "Nasarawa",
    "Niger" => "Niger",
    "Ogun" => "Ogun",
    "Ondo" => "Ondo",
    "Osun" => "Osun",
    "Oyo" => "Oyo",
    "Plateau" => "Plateau",
    "Rivers" => "Rivers",
    "Sokoto" => "Sokoto",
    "Taraba" => "Taraba",
    "Yobe" => "Yobe",
    "Zamfara" => "Zamfara",
  ];

define('NIG_STATE', $states);
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
