<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Symfony\Component\Routing\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/pre-form', 'ApplicationController@preForm');

Route::post('/gen-app-no', 'ApplicationController@generateAppNo');

Route::post('/save-app', 'ApplicationController@store');


Route::group(['middleware' => ['auth'], 'prefix'=>'admin'], function () {
    Route::get('/all-app', 'ApplicationController@admin');
    Route::get('/applicant/view/{id}', 'ApplicationController@adminViewOne');
});


