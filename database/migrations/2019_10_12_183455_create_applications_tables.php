<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->string("app_no")->unique();
            $table->string("applied_before")->nullable();
            $table->string("prev_period")->nullable();
            $table->string("perm_addr")->nullable();
            $table->string("pres_cont_addr")->nullable();
            $table->string("tel_no")->nullable();
            $table->string("email")->nullable();
            $table->string("state")->nullable();
            $table->string("lga")->nullable();
            $table->string("hometown")->nullable();
            $table->string("homeaddr")->nullable();
            $table->string("nationality")->nullable();
            $table->string("dob")->nullable();
            $table->string("marital")->nullable();
            $table->string("nokname")->nullable();
            $table->string("noktel")->nullable();
            $table->string("nokaddr")->nullable();
            $table->string("campus_choice_1")->nullable();
            $table->string("campus_choice_2")->nullable();
            $table->string("campus_choice_3")->nullable();
            $table->string("campus_choice_4")->nullable();
            $table->string("campus_choice_5")->nullable();
            $table->string("campus_choice_6")->nullable();
            $table->string("choicereason")->nullable();
            $table->string("sec_sch_name")->nullable();
            $table->string("sec_sch_date_from")->nullable();
            $table->string("sec_sch_date_to")->nullable();
            $table->string("sec_sch_cert")->nullable();
            $table->string("sec_sch_sub1")->nullable();
            $table->string("sec_sch_sub2")->nullable();
            $table->string("sec_sch_sub3")->nullable();
            $table->string("sec_sch_sub4")->nullable();
            $table->string("sec_sch_sub5")->nullable();
            $table->string("sec_sch_sub6")->nullable();
            $table->string("sec_sch_sub7")->nullable();
            $table->string("sec_sch_sub8")->nullable();
            $table->string("sec_sch_sub9")->nullable();
            $table->string("entry_qualifications")->nullable();
            $table->string("first_post_sec_sch_name")->nullable();
            $table->string("first_post_sec_sch_date_from")->nullable();
            $table->string("first_post_sec_sch_date_to")->nullable();
            $table->string("first_post_sec_sch_title")->nullable();
            $table->string("first_post_sec_sch_cert")->nullable();
            $table->string("first_post_sec_sch_year_obtained")->nullable();
            $table->string("second_post_sec_sch_name")->nullable();
            $table->string("second_post_sec_sch_date_from")->nullable();
            $table->string("second_post_sec_sch_date_to")->nullable();
            $table->string("second_post_sec_sch_title")->nullable();
            $table->string("second_post_sec_sch_cert")->nullable();
            $table->string("second_post_sec_sch_year_obtained")->nullable();
            $table->string("third_post_sec_sch_name")->nullable();
            $table->string("third_post_sec_sch_date_from")->nullable();
            $table->string("third_post_sec_sch_date_to")->nullable();
            $table->string("third_post_sec_sch_title")->nullable();
            $table->string("third_post_sec_sch_cert")->nullable();
            $table->string("third_post_sec_sch_year_obtained")->nullable();
            $table->string("first_emploment_history_date_from")->nullable();
            $table->string("first_emploment_history_date_to")->nullable();
            $table->string("first_emploment_history_post_held")->nullable();
            $table->string("first_emploment_history_employer")->nullable();
            $table->string("first_emploment_history_employer_address")->nullable();
            $table->string("second_emploment_history_date_from")->nullable();
            $table->string("second_emploment_history_date_to")->nullable();
            $table->string("second_emploment_history_post_held")->nullable();
            $table->string("second_emploment_history_employer")->nullable();
            $table->string("second_emploment_history_employer_address")->nullable();
            $table->string("third_emploment_history_date_from")->nullable();
            $table->string("third_emploment_history_date_to")->nullable();
            $table->string("third_emploment_history_post_held")->nullable();
            $table->string("third_emploment_history_employer")->nullable();
            $table->string("third_emploment_history_employer_address")->nullable();
            $table->string("first_unemploment_history_date_from")->nullable();
            $table->string("first_unemploment_history_date_to")->nullable();
            $table->string("first_unemploment_history_where_about")->nullable();
            $table->string("first_unemploment_history_course_title")->nullable();
            $table->string("second_unemploment_history_date_from")->nullable();
            $table->string("second_unemploment_history_date_to")->nullable();
            $table->string("second_unemploment_history_where_about")->nullable();
            $table->string("second_unemploment_history_course_title")->nullable();
            $table->string("second_unemploment_history_class")->nullable();
            $table->string("third_unemploment_history_date_from")->nullable();
            $table->string("third_unemploment_history_date_to")->nullable();
            $table->string("third_unemploment_history_where_about")->nullable();
            $table->string("third_unemploment_history_course_title")->nullable();
            $table->string("third_unemploment_history_class")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications_tables');
    }
}
