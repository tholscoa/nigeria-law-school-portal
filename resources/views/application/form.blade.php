<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">

    <title>NLS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css') }}" rel="stylesheet" name="bootstrap-css">
    <style type="text/css">
    #form {
  background-color: #F9F9F9;
}

.blue {
    color: #2CAFFD;
}

#inputDado {
    border-radius: 30px;
    border: 1px solid #000;
}    </style>
    <script src="{{ asset('cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js') }}"></script>
    <script src="{{ asset('maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        window.alert = function(){};
        var defaultCSS = document.getElementById('bootstrap-css');
        function changeCSS(css){
            if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />');
            else $('head > link').filter(':first').replaceWith(defaultCSS);
        }
        // $( document ).ready(function() {
        //   var iframe_height = parseInt($('html').height());
        //   window.parent.postMessage( iframe_height, 'https://bootsnipp.com');
        // });
    </script>
</head>
<body>
      <!-- Section Form -->
  <section name="form">
    <div class="container pt-5 pb-5">
      <div class="row">
        <div class="col-md-12 text-center">
          <h3 class="text-uppercase">COUNCIL OF LEGAL EDUCATION
            <br>
            <span class="blue">NIGERIAN LAW SCHOOL</span>
          </h3>
          <img src="{{ asset('images/logo.png') }}" alt="Logo">
          <p>HEADQUARTERS, BWARI,<br>P.M.B. 170, GARKI <br>ABUJA-NIGERIA.</p>
          <div class="row">
            <div class="col-md-12">
              <form action="/save-app" method="POST">
                  @csrf
                  <div class="form-row">
                      <div class="form-group col-md-12">
                      <h5 class="text-center"><u>APPLICATION FOR ADMISSION TO THE OCTOBER 2014/2015 BAR PART II COURSE</u></h5>
                      </div>
                      <div class="form-group col-md-12">
                          <h6 class="text-right"><u>Application No: {{ $applicant->app_no }}</u></h6>
                        </div>
                        <div style="display:none">
                            <input type="text" class="form-control" name="app_no" placeholder="{{ $applicant->app_no }}" value="{{ $applicant->app_no }}"></div>

                        <div class="form-group col-md-12">
                         <h6 class="text-left">To be completed by every applicant and returned to the Secretary, Council of Legal Education, Nigerian Law School Headquarters. P.M.B 170 Garki, Abuja together with two coloured Passport size Photographs.</h6>
                        </div>

                    <div class="form-group col-md-12">
                        <h6 class="text-left"><u>SUBMISSION DATE:</u><br>
                        All Forms are to be submitted not later than August 22, 2014.<br>
                        Please note that use of correction fluid to cancel or otherwise alter content of the form may render it invalid. Applicant should therefore study the form carefully before completing it.<br>
                        <u>Names on this Form must correspond with names used in the University and must be maintained at the Nigerian Law School</u>
                        </h6>
                    </div>

                    <!-- PERSONAL DATA BEGINS -->
                    <div class="form-group col-md-12">
                        <h6 class="text-left"><u>PERSONAL DATA:</u></h6>
                    </div>

                    <div class="form-group col-md-1">
                      <label>1.</label>
                    </div>
                    <div class="form-group col-md-11">
                      <input type="name" class="form-control" name="name" placeholder="{{ $applicant->name }}" value="{{ $applicant->name }}">
                    </div>

                    <div class="form-group col-md-1">
                      <label>2. (a)</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                      <label class="text-left">Have you ever applied for Admission into the Nigerian Law School?
                            <input type="checkbox" name="applied_before" value="yes"> Yes ||
                            <input type="checkbox" name="applied_before" value="no" checked> No</label>
                    </div>

                    <div class="form-group col-md-1">
                      <label> (b)</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <input type="text" class="form-control" name="prev_period" value="" placeholder="If yes, state period (s)">
                    </div>

                    <div class="form-group col-md-1">
                      <label>3. (a)</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <input type="text" class="form-control" name="perm_addr" value="" placeholder="Permanent Address:">
                    </div>

                    <div class="form-group col-md-1">
                      <label> (b)</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <input type="text" class="form-control" name="pres_cont_addr" value="" placeholder="Present contact Address: ">
                    </div>

                    <div class="form-group col-md-1">
                      <label> (c)</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <input type="text" class="form-control" name="tel_no" value="" placeholder="Telephone Number (if any)">
                    </div>

                    <div class="form-group col-md-1">
                      <label> (d)</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <input type="email" class="form-control" name="email" value="{{ $applicant->email }}" placeholder="{{ $applicant->email }}" readonly>
                        <label>*The School will use this address for all correspondence with you unless notification of change of address is received.</label>
                    </div>


                    <div class="form-group col-md-1">
                      <label>4. (a)</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                    <label for="inputState">State</label>
                      <select name="state" class="form-control">
                      @foreach($states as $state)
                        <option selected>{{ $state }}</option>
                      @endforeach
                      </select>
                    </div>

                    <div class="form-group col-md-1">
                      <label> (b)</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                    <label for="inputState">Local Government Area</label>
                    <input type="text" class="form-control" name="lga" value="" placeholder="Local Government Area">

                    </div>

                    <div class="form-group col-md-1">
                      <label> (c)</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <input type="text" class="form-control" name="hometown" value="" placeholder="Home Town">
                    </div>

                    <div class="form-group col-md-1">
                      <label> 5.</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <input type="text" class="form-control" name="homeaddr" value="" placeholder="Home Address in Nigeria (NOT P.O.BOX)">
                    </div>

                    <div class="form-group col-md-1">
                      <label> 6.</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <input type="text" class="form-control" name="nationality" value="" placeholder="Nationality">
                        <label>(If Nigerian Citizenship has been acquired by Registration or Naturalization, state below nationality before such Registration or Naturalization and the number of the Registration or Naturalization document. Attach photocopy of the Registration or Naturalization Certificate as the case may be).</label>
                    </div>

                    <div class="form-group col-md-1">
                      <label> 7.</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <input type="date" class="form-control" name="dob" value="" placeholder="Date of Birth">
                    </div>

                    <div class="form-group col-md-1">
                      <label> 8.</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                    <label for="inputState">Marital Status</label>
                      <select name="marital" class="form-control">
                        <option selected>Single</option>
                        <option>Married</option>
                        <option>Divorces</option>
                      </select>
                    </div>

                    <div class="form-group col-md-1">
                      <label> 9.</label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <label>Next of Kin</label>
                        <input type="text" class="form-control" name="nokname" value="" placeholder="Name">
                        <input type="text" class="form-control" name="nokrelationship" value="" placeholder="Relationship">
                        <input type="text" class="form-control" name="noktel" value="" placeholder="GSM No">
                        <input type="text" class="form-control" name="nokaddr" value="" placeholder="Address">
                    </div>

                    <!-- PERSONAL DATA ENDSS -->

                    <!-- Section B DATA BEGINS -->
                    <div class="form-group col-md-12">
                        <h6 class="text-left"><u>SECTION B</u></h6>
                    </div>

                    <div class="form-group col-md-1">
                      <label>10.</label>
                    </div>
                    <div  class="form-group col-md-11 text-left">
                        <label for="campus">The Nigerian Law School operates six (6) Campuses as stated below.
                            Please indicate at least three (3) Campuses of choice in order of preference. Your choice does not necessarily bind the Council.
                        </label>
                            <label>First Choice</label>
                            <select name="campus-choice-1" class="form-control">
                                <option selected>Bwari-Abuja</option>
                                <option>Lagos</option>
                                <option>Enugu</option>
                                <option>Kano</option>
                                <option>Yenagoa</option>
                                <option>Yola</option>
                        </select>


                        <label>Second Choice</label>
                            <select name="campus-choice-2" class="form-control">
                                <option selected>Bwari-Abuja</option>
                                <option>Lagos</option>
                                <option>Enugu</option>
                                <option>Kano</option>
                                <option>Yenagoa</option>
                                <option>Yola</option>
                        </select>

                        <label>Third Choice</label>
                            <select name="campus-choice-3" class="form-control">
                                <option selected>Bwari-Abuja</option>
                                <option>Lagos</option>
                                <option>Enugu</option>
                                <option>Kano</option>
                                <option>Yenagoa</option>
                                <option>Yola</option>
                        </select>

                        <label>Fourth Choice</label>
                            <select name="campus-choice-4" class="form-control">
                                <option selected>Bwari-Abuja</option>
                                <option>Lagos</option>
                                <option>Enugu</option>
                                <option>Kano</option>
                                <option>Yenagoa</option>
                                <option>Yola</option>
                        </select>

                        <label>Fifth Choice</label>
                            <select name="campus-choice-5" class="form-control">
                                <option selected>Bwari-Abuja</option>
                                <option>Lagos</option>
                                <option>Enugu</option>
                                <option>Kano</option>
                                <option>Yenagoa</option>
                                <option>Yola</option>
                        </select>

                        <label>Sixth Choice</label>
                            <select name="campus-choice-6" class="form-control">
                                <option selected>Bwari-Abuja</option>
                                <option>Lagos</option>
                                <option>Enugu</option>
                                <option>Kano</option>
                                <option>Yenagoa</option>
                                <option>Yola</option>
                        </select>
                    </div>

                    <div class="form-group col-md-1">
                      <label> </label>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <label><strong>NB</strong>	Give reasons for your choice and attach relevant documents (If any)</label>
                        <input type="text" class="form-control" name="choicereason" value="" placeholder="Type reason for choice here">
                    </div>
                    <!-- SECTION B ENDS -->

                    <!-- SECONDARY EDUCATION BEGINS -->
                    <div class="form-group col-md-12">
                        <h6 class="text-left">11 a. <u>SECONDARY EDUCATION</u></h6>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <input type="text" class="form-control" name="sec_sch_name" value="" placeholder="Secondary School Attended,  and town/city in which situated">
                        <input type="date" class="form-control" name="sec_sch_date_from" value="" placeholder="From">
                        <input type="date" class="form-control" name="sec_sch_date_to" value="" placeholder="To">
                        <input type="text" class="form-control" name="sec_sch_cert" value="" placeholder="Certificate Obtained">
                    </div>


                    <div class="form-group col-md-12">
                        <h6 class="text-left">ii. Subjects attempted at the WASC, GCE, SSCE, etc Certificate Level and grades</h6>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <input type="text" class="form-control" name="sec_sch_sub1" value="" placeholder="E.g English Language / NECO / A1">
                        <input type="text" class="form-control" name="sec_sch_sub2" value="" placeholder="E.g English Language / NECO / A1">
                        <input type="text" class="form-control" name="sec_sch_sub3" value="" placeholder="E.g English Language / NECO / A1">
                        <input type="text" class="form-control" name="sec_sch_sub4" value="" placeholder="E.g English Language / NECO / A1">
                        <input type="text" class="form-control" name="sec_sch_sub5" value="" placeholder="E.g English Language / NECO / A1">
                        <input type="text" class="form-control" name="sec_sch_sub6" value="" placeholder="E.g English Language / NECO / A1">
                        <input type="text" class="form-control" name="sec_sch_sub7" value="" placeholder="E.g English Language / NECO / A1">
                        <input type="text" class="form-control" name="sec_sch_sub8" value="" placeholder="E.g English Language / NECO / A1">
                        <input type="text" class="form-control" name="sec_sch_sub9" value="" placeholder="E.g English Language / NECO / A1">
                    </div>

                    <div class="form-group col-md-12 text-left">
                        <div>
                          <label> iii. Qualification(s) with which entry was gained to study Law in the University </label>
                        </div>

                        <input type="text" class="form-control" name="entry_qualifications" value="" placeholder="e.g. Diploma, IJMB , JAMB (state precisely)">


                        <label>NOTE: Photocopy of the certificate obtained as stated in 11a (i) & (iii) above MUST be attached and the original MUST be presented at point of registration in the Nigerian Law School.</label>

                    </div>
                    <!-- SECONDARY EDUCATION ENDS -->


                    <!-- POST SECONDARY EDUCATION (NOT YEAR OF PRIVATE STUDY) BEGINS -->
                    <div class="form-group col-md-12">
                        <h6 class="text-left">11 b. <u>POST SECONDARY EDUCATION (NOT YEAR OF PRIVATE STUDY)</u></h6>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <label>(1)</label><br>
                        <label>University Attended Town/city in which situated</label>
                        <input type="text" class="form-control" name="1_post_sec_sch_name" value="" placeholder="University Attended Town/city in which situated">
                        <label>From</label>
                        <input type="date" class="form-control" name="1_post_sec_sch_date_from" value="" placeholder="From">
                        <label>To</label>
                        <input type="date" class="form-control" name="1_post_sec_sch_date_to" value="" placeholder="To">
                        <label>Title</label>
                        <input type="text" class="form-control" name="1_post_sec_sch_title" value="" placeholder="Title">
                        <label>Class of Degree(s)</label>
                        <input type="text" class="form-control" name="1_post_sec_sch_cert" value="" placeholder="e.g B.Tech">
                        <label>Year  Obtained</label>
                        <input type="date" class="form-control" name="1_post_sec_sch_year_obtained" value="" placeholder="Year  Obtained">
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <label>(2)</label><br>
                        <label>University Attended Town/city in which situated</label>
                        <input type="text" class="form-control" name="2_post_sec_sch_name" value="" placeholder="University Attended Town/city in which situated">
                        <label>From</label>
                        <input type="date" class="form-control" name="2_post_sec_sch_date_from" value="" placeholder="From">
                        <label>To</label>
                        <input type="date" class="form-control" name="2_post_sec_sch_date_to" value="" placeholder="To">
                        <label>Title</label>
                        <input type="text" class="form-control" name="2_post_sec_sch_title" value="" placeholder="Title">
                        <label>Class of Degree(s)</label>
                        <input type="2_post_sec_sch_cert" class="form-control" name="2_post_sec_sch_cert" value="" placeholder="e.g B.Tech">
                        <label>Year  Obtained</label>
                        <input type="date" class="form-control" name="2_post_sec_sch_year_obtained" value="" placeholder="Year  Obtained">
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <label>(3)</label><br>
                        <label>University Attended Town/city in which situated</label>
                        <input type="text" class="form-control" name="3_post_sec_sch_name" value="" placeholder="University Attended Town/city in which situated">
                        <label>From</label>
                        <input type="date" class="form-control" name="3_post_sec_sch_date_from" value="" placeholder="From">
                        <label>To</label>
                        <input type="date" class="form-control" name="3_post_sec_sch_date_to" value="" placeholder="To">
                        <label>Title</label>
                        <input type="text" class="form-control" name="3_post_sec_sch_title" value="" placeholder="Title">
                        <label>Class of Degree(s)</label>
                        <input type="text" class="form-control" name="3_post_sec_sch_cert" value="" placeholder="e.g B.Tech">
                        <label>Year  Obtained</label>
                        <input type="date" class="form-control" name="3_post_sec_sch_year_obtained" value="" placeholder="Year  Obtained">
                    </div>
                    <label>NOTE: Photocopy of Law degree certificate awarded MUST be attached if already issued. Production of LL.B certificate is a prerequisite for call to the Nigerian Bar.</label>
                    <!-- POST SECONDARY EDUCATION (NOT YEAR OF PRIVATE STUDY) ENDS -->


                    <!-- EMPLOYMENT BEGINS -->
                    <div class="form-group col-md-12">
                        <h6 class="text-left">12 a. State in chronological order employments and posts held, (if any) since you left Secondary School, Teachers Training College or any other Institution of Higher Learning including a University.</h6>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <label>(1)</label><br>
                        <label>From</label>
                        <input type="date" class="form-control" name="1_emploment_history_date_from" value="" placeholder="From">
                        <label>To</label>
                        <input type="date" class="form-control" name="1_emploment_history_date_to" value="" placeholder="To">
                        <label>Post Title</label>
                        <input type="text" class="form-control" name="1_emploment_history_post_held" value="" placeholder="Post Title">
                        <label>Employer</label>
                        <input type="text" class="form-control" name="1_emploment_history_employer" value="" placeholder="Employer">
                        <label>Address of Employer</label>
                        <input type="text" class="form-control" name="1_emploment_history_employer_address" value="" placeholder="Address of Employer">
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <label>(2)</label><br>
                        <label>From</label>
                        <input type="date" class="form-control" name="2_emploment_history_date_from" value="" placeholder="From">
                        <label>To</label>
                        <input type="date" class="form-control" name="2_emploment_history_date_to" value="" placeholder="To">
                        <label>Post Title</label>
                        <input type="text" class="form-control" name="2_emploment_history_post_held" value="" placeholder="Post Title">
                        <label>Employer</label>
                        <input type="text" class="form-control" name="2_emploment_history_employer" value="" placeholder="Employer">
                        <label>Address of Employer</label>
                        <input type="text" class="form-control" name="2_emploment_history_employer_address" value="" placeholder="Address of Employer">
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <label>(3)</label><br>
                        <label>From</label>
                        <input type="date" class="form-control" name="3_emploment_history_date_from" value="" placeholder="From">
                        <label>To</label>
                        <input type="date" class="form-control" name="3_emploment_history_date_to" value="" placeholder="To">
                        <label>Post Title</label>
                        <input type="text" class="form-control" name="3_emploment_history_post_held" value="" placeholder="Post Title">
                        <label>Employer</label>
                        <input type="3_emploment_history_employer" class="form-control" name="3_emploment_history_employer" value="" placeholder="Employer">
                        <label>Address of Employer</label>
                        <input type="text" class="form-control" name="3_emploment_history_employer_address" value="" placeholder="Address of Employer">
                    </div>
                    <!-- EMPLOYMENT ENDS -->

                    <!-- UNEMPLOYMENT BEGINS -->

                    <div class="form-group col-md-12">
                        <h6 class="text-left">b. State periods of unemployment, private study or period not otherwise accounted for since you left Secondary School, Teachers training college or any other institution of higher learning including a University in chronological order.</h6>
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <label>(1)</label><br>
                        <label>From</label>
                        <input type="date" class="form-control" name="1_unemploment_history_date_from" value="" placeholder="From">
                        <label>To</label>
                        <input type="date" class="form-control" name="1_unemploment_history_date_to" value="" placeholder="To">
                        <label>Your where about</label>
                        <input type="text" class="form-control" name="1_unemploment_history_where_about" value="" placeholder="Your where about">
                        <label>Title of course of study</label>
                        <input type="text" class="form-control" name="1_unemploment_history_course_title" value="" placeholder="Employer">
                        <label>Class</label>
                        <input type="text" class="form-control" name="1_unemploment_history_class" value="" placeholder="Address of Employer">
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <label>(2)</label><br>
                        <label>From</label>
                        <input type="date" class="form-control" name="2_unemploment_history_date_from" value="" placeholder="From">
                        <label>To</label>
                        <input type="date" class="form-control" name="2_unemploment_history_date_to" value="" placeholder="To">
                        <label>Your where about</label>
                        <input type="text" class="form-control" name="2_unemploment_history_where_about" value="" placeholder="Your where about">
                        <label>Title of course of study</label>
                        <input type="text" class="form-control" name="2_unemploment_history_course_title" value="" placeholder="Employer">
                        <label>Class</label>
                        <input type="text" class="form-control" name="2_unemploment_history_class" value="" placeholder="Address of Employer">
                    </div>
                    <div class="form-group col-md-11 text-left">
                        <label>(3)</label><br>
                        <label>From</label>
                        <input type="date" class="form-control" name="3_unemploment_history_date_from" value="" placeholder="From">
                        <label>To</label>
                        <input type="date" class="form-control" name="3_unemploment_history_date_to" value="" placeholder="To">
                        <label>Your where about</label>
                        <input type="text" class="form-control" name="3_unemploment_history_where_about" value="" placeholder="Your where about">
                        <label>Title of course of study</label>
                        <input type="text" class="form-control" name="3_unemploment_history_course_title" value="" placeholder="Employer">
                        <label>Class</label>
                        <input type="text" class="form-control" name="3_unemploment_history_class" value="" placeholder="Address of Employer">
                        <label>NOTE:	Any period which is unaccounted for may result in the rejection of this application.</label>
                    </div>

                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-primary col-md-12">Submit</button>
                    </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>	<script type="text/javascript">

$('input[type="checkbox"]').on('change', function() {
   $(this).siblings('input[type="checkbox"]').prop('checked', false);
});
		</script>
</body>
</html>
