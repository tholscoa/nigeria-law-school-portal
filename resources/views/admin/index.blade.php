
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">

    <title>NLS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css') }}" rel="stylesheet" name="bootstrap-css">
    <style type="text/css">
    #form {
  background-color: #F9F9F9;
}

.blue {
    color: #2CAFFD;
}

#inputDado {
    border-radius: 30px;
    border: 1px solid #000;
}    </style>
    <script src="{{ asset('cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js') }}"></script>
    <script src="{{ asset('maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        window.alert = function(){};
        var defaultCSS = document.getElementById('bootstrap-css');
        function changeCSS(css){
            if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />');
            else $('head > link').filter(':first').replaceWith(defaultCSS);
        }
        // $( document ).ready(function() {
        //   var iframe_height = parseInt($('html').height());
        //   window.parent.postMessage( iframe_height, 'https://bootsnipp.com');
        // });
    </script>
</head>
<body>
      <!-- Section Form -->
  <section name="form">
    <div class="container pt-5 pb-5">
      <div class="row">
        <div class="col-md-12 text-center">
          <h3 class="text-uppercase">COUNCIL OF LEGAL EDUCATION
            <br>
            <span class="blue">NIGERIAN LAW SCHOOL</span>
          </h3>
          <img src="{{ asset('images/logo.png') }}" alt="Logo">
          <p>HEADQUARTERS, BWARI,<br>P.M.B. 170, GARKI <br>ABUJA-NIGERIA.</p>
          <div class="row">
            <div class="col-md-12">
                  <div class="form-row">
                      <div class="form-group col-md-12">
                      <h5 class="text-center"><u>APPLICATION FOR ADMISSION TO THE OCTOBER 2014/2015 BAR PART II COURSE</u></h5>
                      </div>
                    <div class="form-group col-md-12">

                        <table class="table table-striped custab">
    <thead>

        <tr>
            <th>ID</th>
            <th>Application No</th>
            <th>Name</th>
            <th>State</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
            <tbody>
            @foreach($all_apps as $applicant)
            <tr>
                <td>{{ $applicant->id }}</td>
                <td>{{ $applicant->app_no }}</td>
                <td>{{ $applicant->name }}</td>
                <td>{{ $applicant->state }}</td>
                <td class="text-center"><a class="btn btn-info btn-xs" href="{{  url('admin/applicant/view', $applicant->id) }}"><span class="glyphicon glyphicon-edit"></span> View</a> <a  href="{{  url('admin/applicant/delete', $applicant->id) }}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
            </tr>
            @endforeach
    </tbody></table>


                    </div>


            </div>
          </div>
        </div>
      </div>
    </div>
  </section>	<script type="text/javascript">

$('input[type="checkbox"]').on('change', function() {
   $(this).siblings('input[type="checkbox"]').prop('checked', false);
});
		</script>
</body>
</html>
